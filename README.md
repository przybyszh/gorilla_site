## Usage

Install npm dependencies

```sh
 npm install
```

Start the development server

```sh
npm start
```

To build for production

```sh
npm run build
```