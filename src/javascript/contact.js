const $checkbox = document.querySelector('.label--checkbox input');
const $subject = document.querySelector('.label--subject');
const $submit = document.querySelector('.contact__form button[type="submit"]');


class Contact {

  constructor(){
    this.handleCheckbox()
    this.submit()
  }

  handleCheckbox() {
    $checkbox.addEventListener('change', (e) => {
      e.target.checked ? this.showSubject() : this.hideSubject()
    })
  }

  showSubject() {
    $subject.classList.add('active');
  }

  hideSubject() {
    $subject.classList.remove('active');
  }

  submit() {
    $submit.addEventListener('click', (e) => {
      e.preventDefault();
    })
  }
}

export default new Contact($checkbox, $subject, $submit);