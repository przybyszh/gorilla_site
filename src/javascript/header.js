const $burger = document.querySelector('.header__button');
const $menu = document.querySelector('.header__menu');
const $linksMenu = document.querySelectorAll('a[href^="#"]');

class Header {

  constructor(){
    this.toggleClick();
    this.closeMenuOnSelect();
    this.scrolltoElement();
  }

  toggleClick(){
    $burger.addEventListener('click', ()=>{
      this.toggleMenu();
    })
  }

  toggleMenu(){
    $burger.classList.toggle('active');
    $menu.classList.toggle('active');
  }

  closeMenuOnSelect() {
    for (const link of $linksMenu){
      link.addEventListener('click', ()=>{
        $burger.classList.remove('active');
        $menu.classList.remove('active');
      });
    }
  }

  scrolltoElement() {
    for (const link of $linksMenu) {
      link.addEventListener('click', function (e) {
        e.preventDefault();
        const offset = document.querySelector(this.getAttribute('href')).offsetTop - 100;
        window.scrollTo({ top: offset, behavior: "smooth" });
      });
    }
  }
}

export default new Header($burger, $menu, $linksMenu);